import io.restassured.http.ContentType;
import io.restassured.response.Response;

import java.util.*;

import static io.restassured.RestAssured.given;

public class Main {

    private static final String API_KEY = "2129b0d4a034345575dc2dca4e05a2e6";
    private static final String BASE_URL = "http://api.openweathermap.org/data/2.5/weather";

    private static List<String> citiesNames = Arrays.asList("Tel Aviv", "Singapore", "Auckland", "Ushuaia", "Miami",
            "London", "Berlin", "Reykjavik", "Cape Town", "Kathmandu");


    public static void main(String[] args) {
        // get cities
        List<City> cities = getCities(citiesNames);

        // print the temp of shortest daylight time city
        City cityWithShortestDaylight = getCityWithShortestDaylight(cities);
        System.out.println("Shortest daylight time city: \n" +
                cityWithShortestDaylight.toString() + "\n" +
                "temperature: " + cityWithShortestDaylight.getMain().getTemp() + "\n");

        // print the temp of longest daylight time city
        City cityWithLongestDaylight = getCityWithLongestDaylight(cities);
        System.out.println("Longest daylight time city: \n" +
                cityWithLongestDaylight.toString() + "\n" +
                "temperature: " + cityWithLongestDaylight.getMain().getTemp() + "\n");
    }


    // help methods
    private static City getCityWithShortestDaylight(List<City> cities) {
        return cities.stream()
                .min(Comparator.comparing(city -> city.getSys().getDaylight()))
                .orElseThrow(NoSuchElementException::new);
    }

    private static City getCityWithLongestDaylight(List<City> cities) {
        return cities.stream()
                .max(Comparator.comparing(city -> city.getSys().getDaylight()))
                .orElseThrow(NoSuchElementException::new);
    }

    private static List<City> getCities(List<String> citiesNames) {
        List<City> cities = new ArrayList<>();
        citiesNames.forEach(s -> {
            City city = getCity(s);
            if (city != null) {
                cities.add(city);
            }
        });
        return cities;
    }

    private static City getCity(String name) {
        Response resp = given()
                .contentType(ContentType.JSON)
                .queryParam("q", name)
                .queryParam("appid", API_KEY)
                .queryParam("units", "metric")
                .when()
                .get(BASE_URL);

        if (resp.body().jsonPath().getInt("cod") == 200) {
            return resp.body().as(City.class);
        } else {
            return null;
        }
    }

}
