import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class City {
    private Integer id;
    private String name;
    private Sys sys;
    private Main main;

    @JsonIgnoreProperties(ignoreUnknown = true)
    @Data
    public class Sys {
        private String country;
        private Long sunrise;
        private Long sunset;

        public Long getDaylight() {
            if ((sunset != null) && (sunrise != null)) {
                return sunset - sunrise;
            } else {
                return null;
            }
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    @Data
    public class Main {
        private Integer temp;
    }
}




